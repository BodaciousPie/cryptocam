#!/bin/bash

cd "$(dirname "$0")"

mkdir -p build > /dev/null
pushd build > /dev/null 

mkdir -p Debug > /dev/null
pushd Debug > /dev/null

cmake -DCMAKE_BUILD_TYPE=Debug -G "Unix Makefiles" ../../source/

popd > /dev/null

mkdir -p Release > /dev/null
pushd Release > /dev/null

cmake -DCMAKE_BUILD_TYPE=Release -G "Unix Makefiles" ../../source/

popd > /dev/null

mkdir -p Xcode > /dev/null
pushd Xcode > /dev/null

cmake -DCMAKE_BUILD_TYPE=Debug -G Xcode ../../source/

popd > /dev/null
popd > /dev/null
