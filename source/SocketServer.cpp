//
//  SocketServer.cpp
//  SocketServer
//
//  Created by Matthew Cross on 08/03/2017.
//
//

#include <thread>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "SocketServer.hpp"

mc::networking::SocketServer::SocketServer(std::unique_ptr<Socket> upSocket)
:
m_upSocket(std::move(upSocket))
{
	
}

void mc::networking::SocketServer::bind(const uint16_t port)
{
	memset(&m_serverAddress, 0, sizeof(m_serverAddress));
	
	m_serverAddress.sin_family			= AF_INET;
	m_serverAddress.sin_addr.s_addr		= INADDR_ANY;
	m_serverAddress.sin_port			= htons(port);
	
	if (::bind(m_upSocket->getFileDescriptor(), reinterpret_cast<struct sockaddr *>(&m_serverAddress), sizeof(m_serverAddress)) < 0)
	{
		throw std::runtime_error("Failed to bind socket to address");
	}
}

void mc::networking::SocketServer::startListening(std::function<void(int fd)> handlerFunc)
{
	listen(m_upSocket->getFileDescriptor(), 5);
	
	std::cout << "Listening for connections on port " << ntohs(m_serverAddress.sin_port) << std::endl;
	
	while (true)
	{
		struct sockaddr_in cli_addr;
		
		socklen_t clilen;
		clilen = sizeof(cli_addr);
		
		int fd = accept(m_upSocket->getFileDescriptor(), reinterpret_cast<struct sockaddr *>(&cli_addr), &clilen);
		
		char address[INET6_ADDRSTRLEN];
		memset(address, 0, sizeof(address));
		
		int port = 0;
		
		if (cli_addr.sin_family == AF_INET)
		{
			port = ntohs(cli_addr.sin_port);
			inet_ntop(AF_INET, &cli_addr.sin_addr, address, sizeof(address));
		}
		
		std::cout << "Accepted incoming connection: " << address << ":" << port << std::endl;
		
		std::thread([fd, &handlerFunc](){
			try {
				handlerFunc(fd);
			} catch (std::exception& ex) {
				std::cerr << ex.what() << std::endl;
			}
		}).detach();
	}
}

mc::networking::SocketServer::~SocketServer()
{
	m_upSocket->close();
}
