#include <iostream>

#include <string>
#include <assert.h>
#include <iostream>

#include "SocketClient.hpp"

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc.hpp>  // Gaussian Blur
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>  // OpenCV window I/O

using namespace cv;

typedef struct
{
	int64_t m_type = 0;
	int64_t m_width = 0;
	int64_t m_height = 0;
} MatHeaders;

void readMatHeaders(mc::networking::Socket& socket, MatHeaders& headers)
{
	int64_t data[3];
	memset(data, 0, sizeof(data));
	
	socket.readBytes(data, sizeof(data));
	
	int64_t type		= ntohs(data[0]);
	int64_t width		= ntohs(data[1]);
	int64_t height		= ntohs(data[2]);
	
	headers.m_type = type;
	headers.m_width = width;
	headers.m_height = height;
}

int main(int argc, char *argv[])
{
	int rv = 0;
	
	try {
		std::unique_ptr<mc::networking::Socket> upSocket(new mc::networking::CryptoSocket());
		mc::networking::SocketClient client(std::move(upSocket));
		
		client.connectToServer("localhost", 8000);
		
		MatHeaders headers;
		
		readMatHeaders(client.getSocket(), headers);
		
		Size sz(headers.m_width, headers.m_height);
		
		Mat img;
		img = Mat::zeros(sz, headers.m_type);
		int imgSize = img.total() * img.elemSize();
		int key = 0;
		
		if (!img.isContinuous())
		{
			img = img.clone();
		}
		
		std::cout << "Image Size:" << imgSize << std::endl;
		
		namedWindow("CV Video Client");
		
		while (key != 27)
		{
			client.getSocket().readBytes(img.data, imgSize);
			
			imshow("CV Video Client", img);
			std::cout << "Image type: " << img.type() << std::endl;
			std::cout << "Channels: " << img.channels() << std::endl;
			std::cout << "Image size: " << img.size() << std::endl;
			std::cout << "Image depth: " << img.depth() << std::endl;
			std::cout << "Image step: " << img.step << std::endl;
			
			key = waitKey(10);
		}
		
	} catch (std::exception& ex) {
		std::cerr << ex.what() << std::endl;
		
		rv = -1;
	}
	
	return rv;
}
