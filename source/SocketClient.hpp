//
//  SocketClient.hpp
//  SocketServer
//
//  Created by Matthew Cross on 08/03/2017.
//
//

#ifndef SocketClient_hpp
#define SocketClient_hpp

#include <iostream>
#include <string>
#include <netdb.h>
#include <sys/types.h>
#include <memory.h>
#include "Socket.hpp"

namespace mc
{
	namespace networking
	{
		class SocketClient
		{
		private:
			std::unique_ptr<Socket> m_upSocket;
			struct hostent *m_pServer = nullptr;
			
		public:
			SocketClient(std::unique_ptr<Socket> upSocket);
			
			Socket& getSocket() const { return *m_upSocket; };
			
			virtual ~SocketClient();
			
			virtual void connectToServer(const std::string& host, const uint16_t port);
		};
	}
}

#endif /* SocketClient_hpp */
