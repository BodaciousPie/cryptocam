//
//  Socket.hpp
//  SocketServer
//
//  Created by Matthew Cross on 09/03/2017.
//
//

#ifndef Socket_hpp
#define Socket_hpp

#include <stdio.h>
#include <netinet/in.h>

namespace mc
{
	namespace networking
	{
		class Socket
		{
		private:
			int m_fd;
			
		public:
			Socket(int fd);
			Socket();
			virtual ~Socket();
			
			int getFileDescriptor() const {	return m_fd; };
			
			void close();
			
			virtual ssize_t sendBytes(const void *buf, size_t len) const;
			virtual ssize_t readBytes(void *buf, size_t len) const;
		};
		
		class CryptoSocket: public Socket
		{
		public:
			using Socket::Socket;
			
			virtual ssize_t sendBytes(const void *buf, size_t len) const;
			virtual ssize_t readBytes(void *buf, size_t len) const;
		};
	}
}

#endif /* Socket_hpp */
