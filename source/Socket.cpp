//
//  Socket.cpp
//  SocketServer
//
//  Created by Matthew Cross on 09/03/2017.
//
//

#include "Socket.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>

mc::networking::Socket::Socket(int fd)
:
m_fd(fd)
{
	if (fd < 0)
	{
		throw std::runtime_error("Error opening socket");
	}
}

mc::networking::Socket::Socket()
:
mc::networking::Socket(socket(AF_INET, SOCK_STREAM, 0))
{
	
}

mc::networking::Socket::~Socket()
{
	close();
}

void mc::networking::Socket::close()
{
	if (m_fd > 0)
	{
		::close(m_fd);
		m_fd = -1;
	}
}

ssize_t mc::networking::Socket::sendBytes(const void *buf, size_t len) const
{
	ssize_t bytesSent = 0;
	
	if ((bytesSent = send(m_fd, buf, len, 0)) < 0)
	{
		throw std::runtime_error("Failed to send bytes");
	}
	
	return bytesSent;
}

ssize_t mc::networking::Socket::readBytes(void *buf, size_t len) const
{
	ssize_t n = recv(m_fd, buf, len, MSG_WAITALL);// read(m_socketFd, buf, len);
	
	if (n <= 0)
	{
		throw std::runtime_error("Failed reading from socket");
	}
	
	return n;
}

#include <crypto_stream.h>
#include <crypto_hash_sha256.h>

void generateNonce(unsigned char *dst, size_t len, unsigned int msgCount)
{
	//todo
	std::string nonce = "123456789012345678901234";
	memcpy(dst, nonce.data(), len);
}

ssize_t mc::networking::CryptoSocket::sendBytes(const void *buf, size_t len) const
{
	std::string m = "this is my key";
	std::string hash = crypto_hash_sha256(m);
	
	unsigned char key[crypto_stream_KEYBYTES];
	memcpy(key, hash.data(), sizeof(key));
	
	unsigned char nonce[crypto_stream_NONCEBYTES];
	memset(nonce, 0, sizeof(nonce));
	
	//use message number as nonce + something else
	
	unsigned char *pEncrypted = new unsigned char [len];
	memset(pEncrypted, 0, len);
	
	//	int msgCount = 0;
	
	generateNonce(nonce, sizeof(nonce), 0);
	
	crypto_stream_xor(pEncrypted, reinterpret_cast<const unsigned char *>(buf), len, nonce, key);
	
	ssize_t bytesSent = Socket::sendBytes(pEncrypted, len);
	
	delete [] pEncrypted;
	
	return bytesSent;
}

ssize_t mc::networking::CryptoSocket::readBytes(void *buf, size_t len) const
{
	unsigned char *pEncrypted = new unsigned char [len];
	memset(pEncrypted, 0, len);
	
	ssize_t bytesRead = Socket::readBytes(pEncrypted, len);
	
	std::string m = "this is my key";
	std::string hash = crypto_hash_sha256(m);
	
	unsigned char key[crypto_stream_KEYBYTES];
	memcpy(key, hash.data(), sizeof(key));
	
	unsigned char nonce[crypto_stream_NONCEBYTES];
	memset(nonce, 0, sizeof(nonce));
	
	//	int msgCount = 0;
	
	generateNonce(nonce, sizeof(nonce), 0);
	
	crypto_stream_xor(reinterpret_cast<unsigned char *>(buf), pEncrypted, len, nonce, key);
	
	delete [] pEncrypted;
	
	return bytesRead;
}
