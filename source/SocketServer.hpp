//
//  SocketServer.hpp
//  SocketServer
//
//  Created by Matthew Cross on 08/03/2017.
//
//

#ifndef SocketServer_hpp
#define SocketServer_hpp

#include <iostream>

#include <memory.h>

#include "Socket.hpp"

namespace mc
{
	namespace networking
	{
		class SocketServer
		{
		private:
			std::unique_ptr<Socket> m_upSocket;
			struct sockaddr_in m_serverAddress;
			
		public:
			SocketServer(std::unique_ptr<Socket> upSocket);
			
			void bind(const uint16_t port);
			
			void startListening(std::function<void(int fd)> handlerFunc);
			
			virtual ~SocketServer();
		};
	}
}

#endif /* SocketServer_hpp */
