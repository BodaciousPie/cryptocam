#include <string>
#include <iostream>
#include <assert.h>

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc.hpp>  // Gaussian Blur
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>  // OpenCV window I/O

#include "SocketServer.hpp"

using namespace cv;

void sendMatHeaders(const mc::networking::Socket& socket, Mat& img)
{
	std::cout << "Channels: " << img.channels() << std::endl;
	std::cout << "Image type: " << img.type() << std::endl;
	std::cout << "Image size: " << img.size() << std::endl;
	std::cout << "Image depth: " << img.depth() << std::endl;
	std::cout << "Image step: " << img.step << std::endl;
	
//	int channels	= htons(img.channels());
	int64_t type		= htons(img.type());
	int64_t width		= htons(img.size().width);
	int64_t height		= htons(img.size().height);
	
	int64_t data[3] = {type, width, height};
	
	socket.sendBytes(data, sizeof(data));
}

int main(int argc, char *argv[])
{
	VideoCapture defCam(0); //default camera
	
	if (!defCam.isOpened())
	{
		return -1;
	}
	
	auto upCryptoSocket = std::unique_ptr<mc::networking::Socket>(new mc::networking::CryptoSocket());
	
	mc::networking::SocketServer server(std::move(upCryptoSocket));
	
	try {
		server.bind(8000);
		server.startListening([&defCam](int fd) {
			
			mc::networking::CryptoSocket s(fd);
			
			Mat headerImg;
			defCam >> headerImg;
			
			Mat img;
			img = Mat::zeros(headerImg.size(), headerImg.type());
			
			if (!img.isContinuous())
			{
				img = img.clone();
			}
			
			int imgSize = img.total() * img.elemSize();
			
			sendMatHeaders(s, headerImg);
			
			while (true)
			{
				defCam >> img;
				
				if (img.empty())
				{
					std::cout << " < < <  Game over!  > > > ";
					break;
				}
				
				s.sendBytes(img.data, imgSize);
			}
			
			s.close();
		});
		
	} catch (std::exception& ex) {
		std::cerr << ex.what() << std::endl;
	}
	
	return 0;
}



//	Mat img;
//	std::cout << "Image type:" << img.type() << std::endl;
//	img = Mat::zeros(480, 640, CV_8UC1);
//	//make it continuous
//	std::cout << "Image type:" << img.type() << std::endl;
//	if (!img.isContinuous())
//	{
//		img = img.clone();
//		std::cout << "Image type:" << img.type() << std::endl;
//	}
//
//	int imgSize = img.total() * img.elemSize();
//	int bytes = 0;
//
//	namedWindow("CV Video Client", 1);
//
//	while (true)
//	{
//		defCam >> img;
//		std::cout << "Image type:" << img.type() << std::endl;
//
//		imshow("CV Video Client", img);
//
//		waitKey(10);
//	}
//
